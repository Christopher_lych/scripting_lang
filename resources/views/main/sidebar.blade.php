<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="header">Menu</li>
            <li {{Request::is('roles')? 'class=active' : ''}}>
                <a href="{{url('/roles')}}">
                    <span>View Role</span>
                </a>
            </li>
            <li {{Request::is('roles/create')? 'class=active' : ''}}>
                <a href="{{url('/roles/create')}}">
                    <span>Create Role</span>
                </a>
            </li>
        </ul>
    </section>
</aside>