@extends('main.index')

@section('content')
    <div class="box box-info">
        <div class="box-body">
            <div class="row">
                <label for="name" class="col-md-4 control-label">Name</label>
                <div class="col-md-6">
                    {{$role->name}}
                </div>
            </div>
            <div class="row">
                <label for="description" class="col-md-4 control-label">Description</label>
                <div class="col-md-6">
                    {{$role->description}}
                </div>
            </div>
        </div>
    </div>
@endsection